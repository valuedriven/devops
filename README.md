# devops

Projeto de referência com exemplos de práticas DevOps.

Arquitetura básica
- projeto base: Node.js
- backend: Node.js, Express e Sequelize
- frontend: React.js
- banco de dados: Sqlite

Infra
- pipeline: gitlab-ci
- deploy: AWS

Qualidade:
- Eslint
- Jest
- Supertest

Logging:
- Morgan
- Winston
